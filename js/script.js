'use strict';

const form = document.querySelector('.password-form');

const errorText = document.createElement('p');
errorText.style.color = 'red';
error.append(errorText);

form.addEventListener('submit', (e) => {
    e.preventDefault();

    if (pass.value == passCheck.value) {
        const visibleEntry = document.querySelector('.fa-eye-slash[data-visible="entry"]');
        const visibleRepeat = document.querySelector('.fa-eye-slash[data-visible="repeat"]');
        
        resetVisibility(visibleEntry);
        resetVisibility(visibleRepeat);

        errorText.innerText = '';
        setTimeout(alert, 0, 'You are welcome');
        e.target.reset();
    } else {
        errorText.innerText = 'Потрібно ввести однакові значення';
    }
});

form.addEventListener('click', (e) => {
    const target = e.target;

    if (target.nodeName != 'I') return;

    if (target.dataset.visible == 'entry') {
        changeIcon(target);
        showPassword(target);
    } else {
        changeIcon(target);
        showPassword(target);
    }
});



function changeIcon(elem) {
    Array.from(elem.parentElement.children).forEach(child => child.classList.toggle('view'));
}

function showPassword(elem) {
    const inputs = document.querySelectorAll('input');

    inputs.forEach(input => {
        if (input.dataset.input == elem.dataset.visible) {
            if (elem.classList.contains('fa-eye')) {
                input.type='text';
            } else {
                input.type='password';
            }
        }
    });
}

function resetVisibility(elem) {
    if (window.getComputedStyle(elem).display != 'none') {
        changeIcon(elem);
        showPassword(elem);
    }
}